import React,{Component} from 'react';
import { AppRegistry,Modal, View, Image,TouchableHighlight, AsyncStorage,FlatList ,Alert, WebView, Linking,ScrollView, Platform } from 'react-native';
import { Container, Label,Header, Radio,Toast, Input,Item,Form,Title, Content, Footer, FooterTab, Button, Left, Right, Picker,Body, Icon, Text, Spinner } from 'native-base';
import Swiper from 'react-native-swiper';
import {ListItem as ListItemN, List as ListN} from 'native-base';
import {List,ListItem,SearchBar} from 'react-native-elements';
import { Actions, ActionConst } from 'react-native-router-flux';
import comida from '../../assets/comida.png'; 
import cultura from '../../assets/cultura.png'; 
import diversion from '../../assets/diversion.png'; 
import mini from '../../assets/mini.png';
import filter from '../../assets/filter.png';
import price from '../../assets/price.png';
import google from '../../assets/google.png';
import facebook from '../../assets/facebook.png';
import main from '../../assets/main.png';
import { Constants, Location, Permissions,MapView } from 'expo';
import {getPlaces,logOutUser} from '../utils/Controller';
import Camera from '../utils/Camera';

import MapViewDirections from 'react-native-maps-directions';

var style = require('../utils/Styles.js');
const GOOGLE_MAPS_APIKEY='AIzaSyBaAJwtqoIkT0h3Mw41tXeVlWYwTL_lihs';
export default class Index extends Component{
  constructor(props) {
    super(props);
    this.state = { 
        error: '', 
        isLoading: false,
        places:null,
        category:'Todos',
        categories:[
          'Todos',
          'Cultura',
          'Comida',
          'Diversión'
        ],
        route:[],
        routeCoordinates:[],
        location:null,
        footerTab:1,
        categoryModal: false,
    };
  }
  getData(){
    let self = this;
    getPlaces().then(value=>{
      self.setState({places:value.data,isLoading:false})
    }).catch(error=>{
      console.log('error: ', error);
    })
  }
  async startOver(){
    this.setState({isLoading:true,loading:true,refreshing:true,});
    this._getLocationAsync();
    this.getData();
  }

  confirmRemove = (val) =>{
    Alert.alert(
      'Confirmación',
      '¿Está seguro de que desea eliminar el sitio de su ruta?',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'Eliminar', onPress: () => this.removeFromRoute(val)},
      ],
  );
  }
  removeFromRoute = (item) =>{
    let index = item.index;
    let route = this.state.route;
    route.splice(index, 1);
    this.setState({route});
    Toast.show({
      text: "El sitio ha sido eliminado",
      buttonText: "Ok",
      type: "danger",
      duration: 3000
    });
  }

  showModal = () =>{
    this.setState({categoryModal: true});
  }
  hideModal = () =>{
    this.setState({categoryModal: false});
  }
  renderListItem = (val) =>{
    let item = val.item;
    return(
      <ListItemN>
        <Left style={style.mediumFlex}>
          <Image style={style.miniIconList} source={{uri:item.uri}} resizeMode="contain"></Image>
        </Left>
        <Body>
          <Text>{item.name}</Text>
          <Text style={{fontSize:10,color:'#434343'}}>{item.info}</Text>
        </Body>
        <Right>
          <Button transparent onPress={()=> this.confirmRemove(val) }>
            <Icon name='md-close' style={{color:'#f26a45'}}/>
          </Button>
        </Right>
      </ListItemN> 
    );
  }
  renderListItemPlaces = (val) =>{
    let item = val.item;
    return(
      <ListItemN onPress={()=> Actions.info({store:item}) }>
        <Left style={style.mediumFlex}>
          <Image style={style.miniIconList} source={{uri:item.uri}} resizeMode="contain"></Image>
        </Left>
        <Body>
          <Text>{item.name}</Text>
          <Text style={{fontSize:10,color:'#434343'}}>{item.info}</Text>
        </Body>
        <Right>
          <Button transparent onPress={()=> Actions.info({store:item}) }>
            <Icon name='ios-arrow-forward' style={{color:'#f26a45'}}/>
          </Button>
        </Right>
      </ListItemN> 
    );
  }
  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        error: 'Permission to access location was denied',
      });
    }
    let location = await Location.getCurrentPositionAsync({});
    let currentLocation = [location.coords.latitude, location.coords.longitude];
    let routeCoordinates = this.state.routeCoordinates;
    routeCoordinates.push({
      latitude:currentLocation[0],
      longitude:currentLocation[1]
    })
    this.setState({ location:currentLocation, routeCoordinates});
  };

  logout(){
    logOutUser();
    Actions.first();
  }
  logoutModal = ()=>{
    Alert.alert(
        'Confirmación',
        '¿Está seguro de que desea salir?',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'Salir', onPress: () => this.logout()},
        ],
    );
  }
  renderCategories = (value) =>{
    let item = value.item;
    return(
      <ListItemN onPress={()=>this.setState({category:item})}>
        <Left>
          <Text>{item}</Text>
        </Left>
        <Right>
          <Radio selected={this.state.category===item?true:false} />
        </Right>
      </ListItemN>
    )
  }
  addToRoute = async(element) =>{
    this.setState({isLoading:true})
    let route = this.state.route;
    let exists = false;
    await route.map(val=>{
      if(val.id===element.id){
        exists =  true;
        this.setState({isLoading:false})
      }
    })
    if(exists===false){
      route.push(element);
      let routeCoordinates = this.state.routeCoordinates;
      let elementLocation = await element.location;
      let self = this;
      
      setTimeout(()=>{
        if(elementLocation){
        let latitude = parseFloat(elementLocation.split(',')[0]);
        let longitude = parseFloat(elementLocation.split(',')[1]);
        routeCoordinates.push(
          {
            latitude:latitude,
            longitude:longitude
          }
        );
        console.log('routeCoordinates: ', routeCoordinates);
        self.setState({route,routeCoordinates,isLoading:false});
        Toast.show({
          text: "Sitio Agregado",
          buttonText: "Ok",
          type: "success",
          duration: 3000
      });
        }
      },100)
    }
  }
  startRoute = () => {
    if(this.state.route.length>0){

    }
    else{
      Alert.alert('Error','No hay lugares agregados a tu ruta.')
    }
  }
  
  componentWillMount(){
    this.startOver();
  }
render(){
  
  if(this.state.route.length > 0 ){
    let custom = [];
    for(var i = 0; i<this.state.routeCoordinates.length-1;i++){
      let item = {
        from:{
          latitude:this.state.routeCoordinates[i].latitude,
          longitude:this.state.routeCoordinates[i].longitude,
        },
        to:{
          latitude:this.state.routeCoordinates[i+1].latitude,
          longitude:this.state.routeCoordinates[i+1].longitude,
        }
      }
      custom.push(item);
    }
    var directions = custom.map((element,index)=>{
      console.log('element: ', element);
      return(
        <MapViewDirections
          key = {index}
          origin={{latitude:element.from.latitude,longitude:element.from.longitude}}
          destination={{latitude:element.to.latitude,longitude:element.to.longitude}}
          apikey={GOOGLE_MAPS_APIKEY}
          strokeColor='#f26a45'
          strokeWidth={4}
        />);
    })
  };

  var markers = this.state.places!==null &&
    this.state.places.map((element, index)=>{
          let coordenadasArray = element.location.split(',');
          let coordenadas = [parseFloat(coordenadasArray[0]),parseFloat(coordenadasArray[1])];
          if(this.state.category==='Todos'){
            return(
              <MapView.Marker
                key = {index}
                coordinate={{latitude: coordenadas[0],longitude: coordenadas[1]}}
                title={element.name}
                description={element.info}
              >
                <Image 
                  source={element.category==='Comida'?comida:element.category==='Cultura'?cultura:diversion}
                  style={{width:20,height:25}}
                />
                <MapView.Callout containerStyle={style.containerCallout}>
                  <View style={{width:240}}>
                    <View style={style.mainImageContainer}>
                        <Image
                            resizeMode="contain"
                            style={style.mainImageFloating}
                            source={{uri:element.uri}}
                        />
                    </View>
                    <Text style={{fontSize:14,marginVertical:5, textAlign:'center',}}>{element.name}</Text>
                    <Text style={{fontSize:11,marginBottom:5, textAlign:'center',}}>{element.info}</Text>
                    <Button style={{alignSelf:'center'}} onPress={()=>this.addToRoute(element)} >
                      <Text style={{fontSize:14}}>Añadir</Text>
                    </Button>
                  </View>
                </MapView.Callout>
              </MapView.Marker>
            );
          }
          else{
            if(this.state.category === element.category){
              return(
                <MapView.Marker
                  key = {index}
                  coordinate={{latitude: coordenadas[0],longitude: coordenadas[1]}}
                  title={element.name}
                  description={element.info}
                  onLoad={() => this.forceUpdate()}
                >
                  <Image 
                    source={element.category==='Comida'?comida:element.category==='Cultura'?cultura:diversion}
                    style={{width:20,height:25,opacity:0}}
                  />
                  <MapView.Callout containerStyle={style.containerCallout}>
                    <View style={{width:240}}>
                      <View style={style.mainImageContainer}>
                          <Image
                              resizeMode="contain"
                              style={style.mainImageFloating}
                              source={{uri:element.uri}}
                          />
                      </View>
                      <Text style={{fontSize:14,marginVertical:5, textAlign:'center',}}>{element.name}</Text>
                      <Text style={{fontSize:11,marginBottom:5, textAlign:'center',}}>{element.info}</Text>
                      <Button style={{alignSelf:'center'}} onPress={()=>this.addToMyPlaces(element)} >
                        <Text style={{fontSize:14}}>Añadir</Text>
                      </Button>
                    </View>
                  </MapView.Callout>
                </MapView.Marker>
              );
            }
            else{
              return '';
            }
          }
      })

    if(this.state.isLoading == true){
      return(
          <View style={style.centerSpinner}>
              <Spinner color='#f26a45' />
          </View>
      )
    }
    return (
      <Container>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.categoryModal}
          style={style.modal}
        >
          <View style={style.modalContainer}>
            <Header style={{backgroundColor:'transparent',borderWidth:0,borderBottomColor:'transparent'}}>
              <Left />
              <Body />
              <Right>
                <Button transparent onPress={ this.hideModal }>
                  <Icon name='md-close' style={{color:'#f26a45'}}/>
                </Button>
              </Right>
            </Header>
            <Content>
                  <FlatList 
                    data={this.state.categories}
                    renderItem={this.renderCategories}
                    keyExtractor={(item,index) => index+''}
                    extraData={this.state}
                  />
            </Content>
          </View>
        </Modal>
        <Header>
          <Body style={{flexWrap: 'wrap', alignItems: 'flex-start',flexDirection:'row',}}>
              <Image resizeMode="contain" style={style.miniIcon} source={mini} /><Title>RUMBO 360</Title>
          </Body>
          <Right style={{flex:1}}>
            <Button transparent >
              <Image resizeMode="contain" style={style.miniIcon} source={price} />
            </Button>
            <Button transparent onPress={this.showModal}>
              <Image resizeMode="contain" style={style.miniIcon} source={filter} />
            </Button>
            <Button transparent onPress={ this.logoutModal }>
              <Icon name='md-exit' style={{color:'#fff'}}/>
            </Button>
          </Right>
        </Header>
        {
          this.state.footerTab === 1 ?
          <Content>
          {
            this.state.location!==null?
              <Content>
                <MapView
                  style={style.mapFinder}
                  showsUserLocation={true}
                  zoomEnabled = {true}
                  showsMyLocationButton={true}
                  scrollEnabled = {true}
                  apikey={GOOGLE_MAPS_APIKEY}
                  initialRegion={{
                    latitude: parseFloat(this.state.location[0]),
                    longitude: parseFloat(this.state.location[1]),
                    latitudeDelta:  0.02922,
                    longitudeDelta:  0.02421,
                  }}
                >
                {markers}
                {directions}
               
                </MapView>
              <List containerStyle ={{borderTopWidth:0,borderBottomWidth:0,paddingTop:0,marginTop:0}}>
                <ListItemN style={{marginTop:0,paddingTop:10}}>
                  <Body>
                  <Text style={style.cercaTitle}>Mi Ruta</Text>
                  </Body>
                  <Right>
                    <Button transparent onPress={ this.startRoute }>
                      <Icon name='md-map' style={{color:'#f26a45'}}/>
                    </Button>
                  </Right>
                </ListItemN>
                <FlatList
                    data={this.state.route}
                    style={style.flatListFinder}
                    renderItem={this.renderListItem}
                    keyExtractor={(item,index) => index+''}
                    extraData={this.state}
                />
              </List>
            </Content>
            
            :
            <Content>
                <Text>Por favor activa los servicios de localización</Text>
            </Content>
          }
          </Content>
          :
          <Content>
            {
              this.state.footerTab === 2 ?
              <Camera />
              :
              <View>
                <List containerStyle ={{borderTopWidth:0,borderBottomWidth:0,paddingTop:0,marginTop:0}}>
                  <ListItemN style={{marginTop:0,paddingTop:10}}>
                    <Text style={style.cercaTitle}>Lugares</Text>
                  </ListItemN>
                  <FlatList
                      data={this.state.places}
                      renderItem={this.renderListItemPlaces}
                      keyExtractor={(item,index) => index+''}
                  />
                </List>
              </View>
            }
          </Content>
        }
        <Footer>
        <FooterTab>
        <Button vertical active={this.state.footerTab==1?true:false} onPress={()=>this.setState({footerTab:1})}>
            <Icon name="map" />
            <Text>Ruta</Text>
          </Button>
          <Button vertical active={this.state.footerTab==2?true:false} onPress={()=>this.setState({footerTab:2})}>
            <Icon name="camera" />
            <Text>Camara</Text>
          </Button>
          <Button vertical active={this.state.footerTab==3?true:false} onPress={()=>this.setState({footerTab:3})}>
            <Icon name="pin" />
            <Text>Lugares</Text>
          </Button>
        </FooterTab>
      </Footer>
      </Container>
    );
  }
}
