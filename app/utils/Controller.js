import firebase from 'firebase';
export default firebase.initializeApp({
    apiKey: "AIzaSyDW9G93K6rGyO4lkOnY8XrxecMLkBAyYdc",
    authDomain: "rumbo-360.firebaseapp.com",
    databaseURL: "https://rumbo-360.firebaseio.com",
    projectId: "rumbo-360",
    storageBucket: "rumbo-360.appspot.com",
    messagingSenderId: "660463357838"
});
import { Permissions, Notifications,Google } from 'expo';
import {  AsyncStorage,Alert } from 'react-native';
export let db = firebase.database();
export let auth = firebase.auth();
import axios from 'axios';
import {url} from './Config';
export async function getItem(item) {
    try {
      const value = await AsyncStorage.getItem(item);
      return value;
    } catch (error) {
        console.log('error getting item: ', error);
        return null;
    }
    return null;
}
export async function setItem(name,item) {
    try {
      const value = await AsyncStorage.setItem(name,item);
      return value;
    } catch (error) {
        console.log('error setting item: ', error);
        return null;
    }
    return null;
}
export async function removeItem(item) {
    try {
      const value = await AsyncStorage.removeItem(item);
      return value;
    } catch (error) {
        console.log('error removing item: ', error);
        return null;
    }
    return null;
}
export async function logOutUser(){
    try{
        auth.signOut();
        const value = await AsyncStorage.clear()
        return value;
    }
    catch (error){
        console.log('something went wrong')
        return 'error';
    }
}
export function logInUser(email, password) {
    return auth.signInWithEmailAndPassword(email, password);
}
export async function verifyUser (uid){
    let exists = false;
    await getUsers().then(async value=>{
        await Object.values(value.data).map(element=>{
            if(uid===element.uid){
                console.log('element.uid: ', element.uid);
                exists = true;
            }
        })
    });
    return exists;
}
export function registerNewUser(email,password){
    return auth.createUserWithEmailAndPassword(email,password);
}
export function postNewUser(user){
    let options ={
        async: true,
        method: 'POST',
        url:url+'users',
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
        },
        data:user
    };
    return axios(options);
}
export function getPlaces(){
    let options ={
        async: true,
        method: 'GET',
        url:url+'places',
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
        },
    };
    return axios(options);
}
export function googleLogin(){
   
}
export function getUsers(){
    let options ={
        async: true,
        method: 'GET',
        url:url+'users',
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
        },
    };
    return axios(options);
}
export function getUsersPlaces(){
    let options ={
        async: true,
        method: 'GET',
        url:url+'users_places',
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
        },
    };
    return axios(options);
}