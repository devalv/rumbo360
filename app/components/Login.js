import React,{Component} from 'react';
import { AppRegistry, View, Image,TouchableHighlight, AsyncStorage ,Alert, WebView, Linking, Platform } from 'react-native';
import { Container, Label,Header, Input,Item,Form,Title, Content, Footer, FooterTab, Button, Left, Right, Picker,Body, Icon, Text, Spinner } from 'native-base';
import Swiper from 'react-native-swiper';
import { Actions, ActionConst } from 'react-native-router-flux';
import mini from '../../assets/mini.png';
import google from '../../assets/google.png';
import facebook from '../../assets/facebook.png';
import main from '../../assets/main.png';
import { Permissions, Notifications,Google,Facebook } from 'expo';
import {googleLogin,auth} from '../utils/Controller.js';
var style = require('../utils/Styles.js');
const FACEBOOK_APP_ID = '293949897844775';


export default class Login extends Component{
  constructor(props) {
    super(props);
    this.state = { 
        error: '', 
        isLoading: false,
    };
}
toLoginWithMail = () =>{
  Actions.loginMail();
}
googleSign = () =>{
  /*googleLogin().then(value=>{
    console.log('value: ', value);
  }).catch(error=>{
    console.log('error: ', error);
  })*/
}
_handleGoogleLogin = async () => {
  try {
    const result = await Google.logInAsync({androidClientId: '660463357838-82s1pfcb7ccodve5304rdcdnh2tiqduc.apps.googleusercontent.com', iosClientId: '660463357838-d7cri61lamphgfo8q8eujph48v62k8t3.apps.googleusercontent.com', scopes: ["profile", "email"] });

    if (result.type === "success") { 
      const credential = firebase.auth.GoogleAuthProvider.credential( result.idToken, result.accessToken);
      auth.signInWithCredential(credential)
        .then(user => {console.log( user);})
        .catch(error => {console.log(error);});
      return result.accessToken;
    }
    return { cancelled: true };
  } catch (e) {
    return { error: true };
  }
};
 handleFacebookButton=async()=> {
   let self = this;
  const { type, token } = await Facebook.logInWithReadPermissionsAsync(FACEBOOK_APP_ID, {
    permissions: ['public_profile', 'email']
  });
  if (type === 'success') {
    //Firebase credential is created with the Facebook access token.
    const credential = auth.FacebookAuthProvider.credential(token);
    Actions.index();
    auth.signInAndRetrieveDataWithCredential(credential).then(value=>{
      console.log('value: ', value);
      Actions.index();
    }).catch(error => {
      self.setState({ errorMessage: error.message });
      Actions.index();
    });
  }
}
render(){
    if(this.state.isLoading == true){
      return(
          <View style={style.centerSpinner}>
              <Spinner color='#f26a45' />
          </View>
      )
    }
    return (
      <Container>
        <Header>
          <Body style={{flexWrap: 'wrap', alignItems: 'flex-start',flexDirection:'row',}}>
              <Image resizeMode="contain" style={style.miniIcon} source={mini} /><Title>RUMBO 360</Title>
          </Body>
        </Header>
        <Content >
          <View style={style.mainImageContainer}>
              <Image
                  resizeMode="contain"
                  style={style.mainImage}
                  source={main}
              />
          </View>
          <Text style={{marginHorizontal:50,textAlign:'center'}}>Ingresa o regístrate para ganar descuentos visitando los lugares que te gustan</Text>
          <View style={style.br}></View>
          <TouchableHighlight style={style.customLoginBtn} onPress={this._handleGoogleLogin}>
            <View style={{flexWrap: 'wrap', alignItems: 'flex-start',flexDirection:'row',}}>
              <Image resizeMode="contain" style={style.miniIconCustom} source={google} />
              <Text style={{textAlign:'center'}}>Ingresa con Google</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight style={style.customLoginBtn} onPress={this.handleFacebookButton}>
            <View style={{flexWrap: 'wrap', alignItems: 'flex-start',flexDirection:'row',}}>
              <Image resizeMode="contain" style={style.miniIconCustom} source={facebook} />
              <Text style={{textAlign:'center'}}>Ingresa con Facebook</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight style={style.RegisterBtn} onPress={this.toLoginWithMail}>
              <Text style={{textAlign:'center',color:'#fff'}}>Iniciar sesión</Text>
          </TouchableHighlight>
        </Content>
      </Container>
    );
  }
}
