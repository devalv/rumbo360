import React,{Component} from 'react';
import { AppRegistry, View, Image,TouchableHighlight, KeyboardAvoidingView,AsyncStorage ,Alert, WebView, Linking, Platform } from 'react-native';
import { Container, Label,Header, Input,Item,Form,Title, Content, Footer, FooterTab, Button, Left, Right, Picker,Body, Icon, Text, Spinner } from 'native-base';
import Swiper from 'react-native-swiper';
import { Actions, ActionConst } from 'react-native-router-flux';
import mini from '../../assets/mini.png';
import google from '../../assets/google.png';
import facebook from '../../assets/facebook.png';
import main from '../../assets/main.png';
import {registerNewUser,postNewUser} from '../utils/Controller';
var style = require('../utils/Styles.js');

export default class Register extends Component{
  constructor(props) {
    super(props);
    this.state = { 
        error: '', 
        isLoading: false,
        email:'',
        password:'',
        name:'',
    };
}
register = () =>{
  this.setState({isLoading:true});
    let email = this.state.email;
    let self  = this;
    let name = this.state.name;
    let password = this.state.password;
    registerNewUser(email, password).then(async data => {
        console.log('data: ', data);
        try {
            let user = {
              email,
              password,
              name,
              uid:data.user.uid
            }
            postNewUser(user).then(async value=>{
              await AsyncStorage.setItem('uid', data.user.uid);
              self.setState({isLoading:false});
              Actions.index();
            }).catch(err=>{
              console.log('err: ', err);
              self.setState({isLoading:false});
              Alert.alert('Error','Datos incorrectos');
            });
        } catch (error) {
            console.log('error saving uid: ', error);
            self.setState({isLoading:false});
        }
    }).catch((error) => {
        console.log('firebaseError',error);
        self.setState({isLoading:false});
        Alert.alert('Error',error.message);
    });
}
render(){
    if(this.state.isLoading == true){
      return(
          <View style={style.centerSpinner}>
              <Spinner color='#f26a45' />
          </View>
      )
    }
    return (
      <Container>
        <Header>
          <Left>
                <Button transparent onPress={ ()=>{Actions.pop()} }>
                    <Icon name='ios-arrow-back' style={{color:'#fff'}}/>
                </Button>
          </Left>
          <Body style={{flexWrap: 'wrap', alignItems: 'flex-start',flexDirection:'row',}}>
              <Image resizeMode="contain" style={style.miniIcon} source={mini} /><Title>RUMBO 360</Title>
          </Body>
        </Header>
        <Content >
          <View style={style.mainImageContainer}>
              <Image
                  resizeMode="contain"
                  style={style.mainImage}
                  source={main}
              />
          </View>
          <KeyboardAvoidingView style={style.keyboardAvoiding} behavior="padding" enabled>
          <Form>
          <Item floatingLabel style={style.paddingTextCustom}>
            <Input  placeholder='Nombre completo' onChangeText={ (name) => this.setState({ name }) }/>
          </Item>
          <Item floatingLabel style={style.paddingTextCustom}>
            <Input autoCorrect={ false }   keyboardType='email-address'  autoCapitalize = 'none' placeholder='Correo electrónico' onChangeText={ (email) => this.setState({ email }) }/>
          </Item>
          <Item floatingLabel last style={style.paddingTextCustom}>
            <Input placeholder='Contraseña' onChangeText={ (password) => this.setState({ password }) } secureTextEntry />
          </Item>
            {  
              this.state.isLoading ?
              <Spinner size='small' color='black' />
              :
              <View>
                <Button block  style={style.paddingTextCustom} onPress={ this.register }>
                  <Text>Crear cuenta</Text>
                </Button>
              </View>
            }
        </Form>
        </KeyboardAvoidingView>
        </Content>
      </Container>
    );
  }
}
