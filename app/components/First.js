import React,{Component} from 'react';
import { AppRegistry, View, Image, AsyncStorage ,Alert, WebView, Linking, Platform } from 'react-native';
import { Container, Label,Header, Input,Item,Form,Title, Content, Footer, FooterTab, Button, Left, Right, Picker,Body, Icon, Text, Spinner } from 'native-base';
import Swiper from 'react-native-swiper';
import { Actions, ActionConst } from 'react-native-router-flux';
import mini from '../../assets/mini.png';
import main from '../../assets/main.png';
import tutorial1 from '../../assets/tutorial1.png';
import tutorial2 from '../../assets/tutorial2.png';
import tutorial3 from '../../assets/tutorial3.png';
var style = require('../utils/Styles.js');

export default class First extends Component{
    constructor(props) {
        super(props);
        this.state = { 
            error: '', 
            isLoading: false,
        };
    }
    startNow(){
        Actions.login();
    }
    render(){
        if(this.state.isLoading == true){
            return(
                <View style={style.centerSpinner}>
                    <Spinner color='#f26a45' />
                </View>
            )
        }
        return (
            <Swiper style={style.wrapper} loop={false}>
                <Container style={style.view1}>
                    <Header>
                    <Body style={{flexWrap: 'wrap', alignItems: 'flex-start',flexDirection:'row',}}>
                        <Image resizeMode="contain" style={style.miniIcon} source={mini} /><Title>RUMBO 360</Title>
                    </Body>
                    </Header>
                    <Content >
                        <View style={style.textContainer}>
                            <Text style={style.whiteTitleCustom}>¿Quieres ganar descuentos conociendo lugares?</Text>
                            <View style={style.br}></View>
                            <Text style={style.whiteSmallTitle}>Con Rumbo 360 los puedes hacer y es así de fácil</Text>
                            <View style={style.mainImageContainer}>
                                <Image
                                    resizeMode="contain"
                                    style={style.mainImage}
                                    source={tutorial1}
                                />
                            </View>
                            <Text style={style.whiteSmallTitle}>1. En el mapa selecciona los lugares que deseas conocer</Text>
                        </View>
                    </Content>
                </Container>
                <Container style={style.view1}>
                    <Header>
                    <Body style={{flexWrap: 'wrap', alignItems: 'flex-start',flexDirection:'row',}}>
                        <Image resizeMode="contain" style={style.miniIcon} source={mini} /><Title>RUMBO 360</Title>
                    </Body>
                    </Header>
                    <Content >
                        <View style={style.textContainer}>
                            <Text style={style.whiteTitleCustom}>¿Quieres ganar descuentos conociendo lugares?</Text>
                            <View style={style.br}></View>
                            <Text style={style.whiteSmallTitle}>Con Rumbo 360 los puedes hacer y es así de fácil</Text>
                            <View style={style.mainImageContainer}>
                                <Image
                                    resizeMode="contain"
                                    style={style.mainImage}
                                    source={tutorial2}
                                />
                            </View>
                            <Text style={style.whiteSmallTitle}>2. Añádelo como tu destino, vísitalo y acomula puntos</Text>
                        </View>
                    </Content>
                </Container>
                <Container style={style.view1}>
                    <Header>
                    <Body style={{flexWrap: 'wrap', alignItems: 'flex-start',flexDirection:'row',}}>
                        <Image resizeMode="contain" style={style.miniIcon} source={mini} /><Title>RUMBO 360</Title>
                    </Body>
                    </Header>
                    <Content >
                        <View style={style.textContainer}>
                            <Text style={style.whiteTitleCustom}>¿Quieres ganar descuentos conociendo lugares?</Text>
                            <View style={style.br}></View>
                            <Text style={style.whiteSmallTitle}>Con Rumbo 360 los puedes hacer y es así de fácil</Text>
                            <View style={style.mainImageContainer}>
                                <Image
                                    resizeMode="contain"
                                    style={style.mainImage}
                                    source={tutorial2}
                                />
                            </View>
                            <Text style={style.whiteSmallTitle}>3. Visualiza tus puntos y obtén tus descuentos</Text>
                        </View>
                    </Content>
                </Container>
                <Container style={style.view2}>
                    <Content >
                        <View style={style.br}></View>
                        <View style={style.textContainer}>
                            <Text style={style.whiteTitleCustom}>Te damos la Bienvenida a</Text>
                            <View style={style.br}></View>
                            <View style={style.mainImageContainer}>
                                <Image
                                    resizeMode="contain"
                                    style={style.mainImage}
                                    source={main}
                                />
                            </View>
                            <View style={style.br}></View>
                            <View style={style.br}></View>
                            <Button full block style={style.button} onPress={this.startNow.bind(this) }>
                                <Text style={{fontSize:12}}>A CADA PASO TUS GUSTOS TE GUÍAN</Text>
                            </Button>
                        </View>
                    </Content>
                </Container>
            </Swiper>
        );
    }
}
